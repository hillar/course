#/bin/sh

PARTDIR=${1:-../content}
PARTS=${2:-3}
CHAPTERS=${3:-4}
SECTIONS=${4:-3}
SUBSECTIONS=${5:-5}

maketemplate(){
  echo ${SECTIONS}
  echo "---" > $1
  echo "title: $2" >> $1
  echo "annotation: $3" >> $1
  echo "start: xx:00" >> $1
  echo "end: xx:45" >> $1
  echo "---" >> $1
  for s in $(seq 1 ${4})
  do
    echo "" >> $1
    echo "# $2 section ${s}" >> $1
    echo "" >> $1
    echo "some text, or bullets .. " >> $1
    for ss in $(seq 1 ${5})
    do
      echo "*  subsection ${s}.${ss}" >> $1
    done
    echo "" >> $1
    for ss in $(seq 1 ${5})
    do
      echo "" >> $1
      echo "## subsection ${s}.${ss}" >> $1
      echo "" >> $1
      echo " $2 some text, or bullets .. " >> $1
      echo "" >> $1
    done

  done
  cat $1
}

for p in $(seq 1 ${PARTS})
do
  pd="${PARTDIR}/${p}"
  [ -d ${pd} ] || mkdir -p ${pd}
  cf="${pd}/00-chapter.md"

  maketemplate ${cf} "part ${p}" "hommikune sissejuhatus ${p}" 1 ${CHAPTERS}
  for c in $(seq 1 ${CHAPTERS})
  do
    cf="${PARTDIR}/${p}/${c}0-chapter.md"
    maketemplate ${cf} "chapter ${c}" "tund ${p} ${c}" ${SECTIONS} ${SUBSECTIONS}
  done
  cf="${PARTDIR}/${p}/99-chapter.md"
  maketemplate ${cf} "outro ${p}" "päeva kokkuvõte ${p}" 1 ${CHAPTERS}
done
