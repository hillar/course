#/bin/sh

PARTDIR=${1:-../content}
PREFIX=${2:-part-}
PARTS=3
# 3 days
CHAPTERS=4
# intro + 4 hours + outro = 6 hours
SECTIONS=3
# 3 sections (15 minutes) per hour
SUBSECTIONS=5
# 5 subsections (3 minutes) per 15 minute sections
bullets=3
# 3 bullets per one 3 minute subsection
FIRST=09
BEFORENOON=$(seq 10 11 )
AFTERNOON=$(seq 14 15 )
LAST=16


maketemplate(){

  echo "---" > $1
  echo "title: $2" >> $1
  echo "annotation: $3" >> $1
  echo "start: xx:00" >> $1
  echo "end: xx:45" >> $1
  echo "---" >> $1
  for s in $(seq 1 ${4})
  do
    echo "" >> $1
    echo "# $2 section ${s}" >> $1
    echo "" >> $1
    echo "some text, or bullets .. " >> $1
    for ss in $(seq 1 ${5})
    do
      echo "*  subsection ${s}.${ss}" >> $1
    done
    echo "" >> $1
    for ss in $(seq 1 ${5})
    do
      echo "" >> $1
      echo "## subsection ${s}.${ss}" >> $1
      echo "" >> $1
      echo " $2 some text, or bullets .. " >> $1
      echo "" >> $1
    done
  done
  cat $1
}

makeHour(){
  echo $1
  echo $2
  echo "---" > $1
  echo "title: lesson $2:00" >> $1
  echo "annotation: $2" >> $1
  echo "start: ${2}:00" >> $1
  echo "end: ${2}:45" >> $1
  echo "---" >> $1
  for s in $(seq 00 15 30)
  do
    echo "" >> $1
    echo "# ${2}:${s} section (15 minutes)" >> $1
    echo "" >> $1
    echo " some text .." >> $1
    echo "" >> $1
    for ss in $(seq 1 ${4})
    do
      echo "" >> $1
      echo "## subsection  ${ss}" >> $1
      echo "" >> $1
      for ss in $(seq 1 3)
      do
        echo "*  item " >> $1
      done
      echo "" >> $1
    done
  done


  cat $1

}


for p in $(seq 1 ${PARTS})
do
  pd="${PARTDIR}/${PREFIX}${p}"
  [ -d ${pd} ] || mkdir -p ${pd}
  f="${pd}/${FIRST}-hour.md"
  makeHour ${f} ${FIRST} ${SECTIONS} ${SUBSECTIONS}
  for h in ${BEFORENOON}
  do
    f="${pd}/${h}-hour.md"
    makeHour ${f} ${h} ${SECTIONS} ${SUBSECTIONS}
  done
  for h in ${AFTERNOON}
  do
    f="${pd}/${h}-hour.md"
    makeHour ${f} ${h} ${SECTIONS} ${SUBSECTIONS}
  done
  f="${pd}/${LAST}-hour.md"
  makeHour ${f} ${LAST} ${SECTIONS} ${SUBSECTIONS}

done
