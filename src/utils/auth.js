const ADMINS = process.env.ADMINS ? process.env.ADMINS.split(',') : []

const sanitize_user = obj => obj && ({
	uid: obj.uid,
	username: obj.username,
	name: obj.name,
	avatar: obj.avatar,
	admin: ADMINS.includes(obj.uid)
});

const MOCK = process.env.MOCK

export const authenticate = () => {
	return async (req, res, next) => {
		if (MOCK) {
			const mock = {
					  'x-ssl-client-s-dn-cn': 'AARELAID,HILLAR,36712316013',
					  'x-ssl-client-s-dn-serial': '36712316013',
					}
			for (const key of Object.keys(mock)) req.headers[key] = mock[key]
		}

		if (req.headers['x-ssl-client-s-dn-serial'] && req.headers['x-ssl-client-s-dn-cn']) {
			const id = req.headers['x-ssl-client-s-dn-serial'].replace('PNOEE-','')
			const uid = id
			const bittes = req.headers['x-ssl-client-s-dn-cn'].split(',')
			const username = bittes[1] + ' '+ bittes[0]
			const name = username
			const avatar = ''
			req.user = sanitize_user({id,uid,username,name,avatar})
		}
		next();
	};
};
