import fs from 'fs'
import path from 'path'
import { extract_frontmatter } from './markdown.js'

export function getParts(p){
  const CONTENTDIR = process.CONTENTDIR
  let parts = []
  try {
    const names = fs.readdirSync(CONTENTDIR,{ withFileTypes: true }).filter(e => e[0] !== '.' && e.isDirectory()).map(e=>e.name)
    for (const name of names) {
      let title = name
      let annotation
      const partdir = path.join(CONTENTDIR,name)
      let firstname
      try {
        firstname = fs.readdirSync(partdir).filter(file => file[0] !== '.' && path.extname(file) === '.md').shift()
      } catch (error) {
        console.error('--- getParts ---')
        console.error(error)
      }
      if (firstname) {
        let md
        try {
          md = fs.readFileSync(path.join(partdir,firstname)).toString()
        } catch (error) {
          console.error('--- getParts ---')
          console.error(error)
        }
        if (md) {
          const {metadata, content} = extract_frontmatter(md)
          if (metadata ) {
            if (metadata.title) title = metadata.title
            if (metadata.annotation) annotation = metadata.annotation
          }
        }
      }
      parts.push({name,title,annotation})
    }
  } catch ( error)  {
    console.error('--- getParts ---')
    console.error(error)
  }
  return parts
}
