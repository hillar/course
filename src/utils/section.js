import fs from 'fs'
import path from 'path'
import { extract_frontmatter } from './markdown.js'

export function getSection(name,section){
  let md
  try {
    md = fs.readFileSync(path.join(process.CONTENTDIR,name,section+'.md')).toString()
  } catch (error) {
    console.error(error)
  }
  return md
}
