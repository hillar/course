import fs from 'fs'
import path from 'path'
import { extract_frontmatter, extract_metadata, link_renderer } from './markdown.js';
import { make_session_slug_processor } from '@sveltejs/site-kit/utils/slug';
import { parseChapterSections } from './markdown.js'

export function getChapters(part){
  const CONTENTDIR = process.CONTENTDIR
  const chapters = []
      let title = part
      let annotation
      const partdir = path.join(CONTENTDIR,part)
      let mds
      try {
        mds = fs.readdirSync(partdir).filter(f => path.extname(f) === '.md').map(f=> path.basename(f,'.md'))
      } catch (error) {
        console.error('--- getChapters ---')
        console.error(error)
      }
      for (const md of mds) {
        const chapter = getChapter(part,md)
        if (chapter) chapters.push(chapter)
      }
      let slug_duppes = 0
      if (chapters && chapters.length) {
        for (const chapter of chapters) {
          try {
            chapter.slug = make_slug(chapter.metadata.title)
          } catch (error) {
            slug_duppes ++
            chapter.slug = make_slug(chapter.metadata.title+'_duplicate_'+Math.random())
          }
          chapter.sections = parseChapterSections(chapter.content)
        }
      }
  return chapters
}


const SLUG_PRESERVE_UNICODE = false;
const SLUG_SEPARATOR = '_';

const make_slug = make_session_slug_processor({
		preserve_unicode: SLUG_PRESERVE_UNICODE,
		separator: SLUG_SEPARATOR
	});

export function getChapter(part,md){
  const CONTENTDIR = process.CONTENTDIR
  let raw
  const filename = path.join(CONTENTDIR,part,md)+'.md'
  try {
    raw = fs.readFileSync(filename).toString()
  } catch (error) {
    console.error('--- getChapter ---')
    console.error(error)
  }
  if (raw) {
    const {metadata, content} = extract_frontmatter(raw)
    return {md:path.basename(filename,'.md'), metadata, content}
  }
}
