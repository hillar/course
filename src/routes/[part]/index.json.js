import send from '@polka/send';
import {getParts} from '../../utils/parts.js'
import {getChapters} from '../../utils/chapter.js'
let json;

export function get(req, res) {
	const { part } = req.params;
	let parts = getParts().filter(p => (p.name === part))
	if (parts && parts.length === 1) {
		let chapters = getChapters(parts[0].name)
		send(res, 200, JSON.stringify(chapters));
	} else {
		res.writeHead(404);
		res.end()
	}
}
