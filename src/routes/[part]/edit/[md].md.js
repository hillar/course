import send from '@polka/send';
import fs from 'fs'
import path from 'path'
import Busboy from 'busboy';

import {getParts} from '../../../utils/parts.js';
import {getSection} from '../../../utils/section.js';

export function get(req, res) {
  const { part, md } = req.params;
  if (!(req.user && req.user.admin)) send(res, 404)
  else if (!(part && md)) send(res, 404)
	let parts = getParts().filter(p => (p.name === part))
	if (parts && parts.length === 1) {
    let section = getSection(parts[0].name,md)
		if (section) send(res, 200, section)
    else {
      res.writeHead(404);
  		res.end()
    }
  } else {
    res.writeHead(404);
		res.end()
  }
}

export async function post(req,res) {
  const { part, md } = req.params;
  if (!(req.user && req.user.admin)) send(res, 404)
  else if (!(part && md)) send(res, 404)
  else await new Promise( resolve => {
  const busboy = new Busboy({ headers: req.headers });
  const fields = {}
  busboy.on('field', function(fieldname, val, fieldnameTruncated, valTruncated, encoding, mimetype) {
    fields[fieldname] = val
  });
  busboy.on('finish', function() {
    const CONTENTDIR = process.CONTENTDIR
    const filename = path.join(CONTENTDIR,part,md+'.md')
    if (fields && fields.txt && fields.txt.trim().length) {
      fs.writeFile(filename, fields.txt, (err) => {
        if (err) {
          console.error(error)
          console.error(filename)
          send(res, 500);
        } else {
          console.log('saved: ', filename)
          res.writeHead(303, { Connection: 'close', Location: `/${part}` });
          res.end()
        }
        resolve();
      });
    } else if  (fields && fields.delete && fields.delete.trim().length) {
      fs.unlink(filename, (err) => {
        if (err) {
          console.error(error)
          console.error(filename)
          send(res, 500);
        } else {
          console.log('deleted: ', filename)
          // check is there any files left,
          // if not, delete part dir also
          const partdir = path.join(CONTENTDIR,part)
          const names = fs.readdirSync(partdir,{ withFileTypes: true }).filter(e => e[0] !== '.' && !e.isDirectory()).map(e=>e.name)
          if (!(names && names.length)) {
            try {
              fs.rmdirSync(partdir)
              console.log('deleted par dir: ', partdir)
            } catch (error) {
              console.error('cant delete part dir')
              console.error(error)
            }
            res.writeHead(303, { Connection: 'close', Location: `/${part}` });
            res.end()

          } else {
          res.writeHead(303, { Connection: 'close', Location: `/${part}` });
          res.end()}
        }
        resolve();
      });

    } else {
      send(res, 415);
      resolve();
    }
  });
  req.pipe(busboy);
})

}

export async function del(req,res) {
  const { part, md } = req.params;
  if (!(req.user && req.user.admin)) send(res, 404)
  else if (!(part && md)) send(res, 404)
  else await new Promise( resolve => {
  const busboy = new Busboy({ headers: req.headers });
  const fields = {}
  busboy.on('field', function(fieldname, val, fieldnameTruncated, valTruncated, encoding, mimetype) {
    fields[fieldname] = val
  });
  busboy.on('finish', function() {
    if (fields && fields.delete && fields.delete.trim().length) {
      const CONTENTDIR = process.CONTENTDIR
      const filename = path.join(CONTENTDIR,part,md+'.md')
      fs.unlink(filename, (err) => {
        if (err) {
          console.error(error)
          console.error(filename)
          send(res, 500);
        } else {
          console.log('deleted: ', filename)
          res.writeHead(303, { Connection: 'close', Location: `/${part}` });
          res.end()
        }
        resolve();
      });

    } else {
      send(res, 415);
      resolve();
    }
  });
  req.pipe(busboy);
})

}
