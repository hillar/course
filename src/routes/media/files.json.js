import send from '@polka/send';
import Busboy from 'busboy';
import fs from 'fs';
import path from 'path'


export function get(req, res) {
  const MEDIADIR = process.MEDIADIR
  try {
    const names = fs.readdirSync(MEDIADIR,{ withFileTypes: true }).filter(e => e[0] !== '.' && !e.isDirectory()).map(e=>e.name)
	   send(res, 200, JSON.stringify(names));
  } catch (error) {
    console.error('--- get media files ---')
    console.error(error)
    send(res, 500);
  }
}
