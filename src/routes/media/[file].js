import send from '@polka/send';
import Busboy from 'busboy';
import fs from 'fs';
import path from 'path'


export function get(req, res) {
  const MEDIADIR = process.MEDIADIR
  const {file} = req.params
  try {
     send(res, 200, fs.readFileSync(path.join(MEDIADIR,file)));
  } catch (error) {
    console.error('--- get media file ---')
    console.error(error)
    send(res, 500);
  }
}

export function del(req, res) {
  const MEDIADIR = process.MEDIADIR
  if (!(req.user && req.user.admin)) send(res, 401);
  else {
    const {file} = req.params
    try {
       fs.unlinkSync(path.join(MEDIADIR,file))
       send(res, 200);
    } catch (error) {
      console.error('--- get media file ---')
      console.error(error)
      send(res, 500);
    }
  }
}
