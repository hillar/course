import send from '@polka/send';
import Busboy from 'busboy';
import fs from 'fs';
import path from 'path'
import {getParts} from '../utils/parts.js';

export function get(req, res) {
	send(res, 200, JSON.stringify(getParts()));
}

export async function post(req, res) {
  const { slug } = req.params;
  await new Promise( resolve => {
    const busboy = new Busboy({ headers: req.headers });
		const files = {}
		busboy.on('file', function(fieldname, file, filename, encoding, mimetype) {
		 console.log('File [' + fieldname + ']: filename: ' + filename + ', encoding: ' + encoding + ', mimetype: ' + mimetype);
		 files[filename] = []
		 file.on('data', function(data) {
			 files[filename].push(data)
		 });
		 file.on('end', function() {
		 });
	 });
    const fields = {}
    busboy.on('field', function(fieldname, val, fieldnameTruncated, valTruncated, encoding, mimetype) {
      fields[fieldname] = val
    });
    busboy.on('finish', function() {
      if (fields && fields.name && fields.title) {
        const CONTENTDIR = process.CONTENTDIR
        const partdir = path.join(CONTENTDIR,fields.name)
				if (!fs.existsSync(partdir)) {
					try {
						fs.mkdirSync(partdir,{recursive:true})
						console.log('created: ' + partdir)
						const filename = path.join(partdir,'00-intro.md')
						const txt = `---
title:${fields.title}
annotation:${fields.annotation}
---

# header 1

* \`# \` marks horisontal slide

notes:
- this will not shown in slides
- to view notes press "S" to open presenter view in new window

## header 2

* \`##(+) \` marks vertical slide

`
						try {
							fs.writeFileSync(filename, txt)
							console.log('wrote: ', filename)
							res.writeHead(303, { Connection: 'close', Location: `/${fields.name}/edit/00-intro` });
	            res.end()
							resolve();
						} catch (error) {
							console.error(error)
							console.error('failed to write ' + filename)
							send(res, 500);
							resolve();
						}
					} catch (error) {
						console.error(error)
						console.error('failed to create ' + partdir)
						send(res, 500);
						resolve();
					}
				}
			} else if (files && Object.keys(files).length) {
				const MEDIADIR = process.MEDIADIR
				for (const filename of Object.keys(files)){
					if (!(filename.trim().length && files[filename].length)) continue
					try {
						fs.writeFileSync(path.join(MEDIADIR,filename), Buffer.concat(files[filename]))
						console.log('wrote: ', filename)

					} catch (error) {
						console.error(error)
						console.error('failed to write ' + filename)
					}
				}
				res.writeHead(303, { Connection: 'close', Location: `/` });
				res.end()
				resolve();

      } else {
        send(res, 415);
        resolve();
      }
    });
    req.pipe(busboy);
  })
}
