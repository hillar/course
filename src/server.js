import fs from 'fs'
import path from 'path'
import sirv from 'sirv';
import polka from 'polka';
import {authenticate} from './utils/auth.js'
import compression from 'compression';
import * as sapper from '@sapper/server';

const dev = process.env.NODE_ENV === 'development';

const PORT = process.env.PORT ? process.env.PORT : 3000
const ROOTDIR = process.env.ROOTDIR ? process.env.ROOTDIR : process.cwd()
const CONTENTDIR = process.env.CONTENTDIR ? process.env.CONTENTDIR : 'content'
const MEDIADIR = process.env.MEDIADIR ? process.env.MEDIADIR : 'media'

const contentdir = path.resolve(path.join(ROOTDIR,CONTENTDIR))
if (!fs.existsSync(contentdir)) {
	console.error('missing: ' + contentdir)
	try {
		fs.mkdirSync(contentdir,{recursive:true})
		console.log('created: ' + contentdir)
	} catch (error) {
		console.error(error)
		console.error('failed to create ' + contentdir)
		process.exit(-1)
	}
}
process.CONTENTDIR = contentdir

const mediadir = path.resolve(path.join(ROOTDIR,MEDIADIR))
if (!fs.existsSync(mediadir)) {
	console.error('missing: ' + mediadir)
	try {
		fs.mkdirSync(mediadir,{recursive:true})
		console.log('created: ' + mediadir)
	} catch (error) {
		console.error(error)
		console.error('failed to create ' + mediadir)
		process.exit(-1)
	}
}
process.MEDIADIR = mediadir

polka()
	.use(
		authenticate(),
		compression({ threshold: 0 }),
		sirv('static', { dev }),
		sapper.middleware({
			session: (req, res) => ({
				user: req.user
			})
		})
	)
	.listen(PORT, err => {
		if (err) console.log('error', err);
	});
