FROM node:12

WORKDIR /opt/course
RUN npm install degit && npx degit https://gitlab.com/hillar/course --force && npm install && npm run build

ARG ADMINS
ENV ADMINS $ADMINS

EXPOSE 3000
CMD npm run start
