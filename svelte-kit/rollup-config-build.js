import { nodeResolve } from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
export default {
  input: 'build/index.js',
  output: {
    file: 'build/server.mjs',
    format: 'esm'
  },
  plugins: [ commonjs(), nodeResolve() ]
}
