import preprocess from 'svelte-preprocess';
import adapternode from '@sveltejs/adapter-node';

console.dir(adapternode)

/** @type {import('@sveltejs/kit').Config} */
const config = {
	preprocess: preprocess(),
	kit: {
		target: '#svelte',
		adapter: adapternode()
	}
};

export default config;
