import  { EndpointOutput, Show } from '$lib/types'
import marked from "$lib/tools/marked-2.0.5/marked.esm.js"

import {resolve, dirname, basename, join} from 'path'
import {mkdir, writeFile, readFile, access, readdir, rename} from 'fs'

const markdown2html = (s = '') => marked(s)

function renameFile(o:string,n:string):Promise<boolean|error> {
  return new Promise ( (res) => {
    rename(o, n, (err) => {
      if (err) res(err)
      else res(true)
    })

  })
}

function saveFile(filename:string,data:any):Promise<boolean|Error>{
  return new Promise( (res) => {
    writeFile(filename, data, (err) => {
      if (err) res(err)
      else res(true)
    })
  })
}

function loadFile(name:string):Promise<string|Error>  {
  return new Promise ( (res) => {
    readFile(name,'utf8',(err, data) => {
      if (err) res(err);
      else res(data)
    })
  })
}

function loadFileList(name:string):Promise<string[]|Error>  {
  return new Promise ( (res) => {
    readdir(name,(err, files) => {
      if (err) res(err);
      else res(files.filter(x => !x.startsWith('__')))
    })
  })
}

function ensureDir(name):Promise<boolean|Error> {
  return new Promise((res)=>{
    const pth = resolve(dirname(name))
    access(pth, (err) => {
      if (err) {
        mkdir(pth,{recursive:true}, (err) => {
          if (err) res(err)
          else res(true)
        })
      } else res(true)
    });
  })
}


async function save(name:string,show:Show):EndpointOutput {
  show.filename = name
  let result = await ensureDir(name)
  if (!((result instanceof Error ))) result = await saveFile(name, JSON.stringify(show,null,2))
  let status = 200
  let body = ''
  if (result instanceof Error ) {
    console.error(result)
    status = 500
    body =result.toString()
  } else console.log(new Date(),'saved',name)
  return {status, body}
}

async function load(name:string):EndpointOutput {
  const result = await loadFile(name)
  let status = 200
  let body = result
  if (result instanceof Error) {
    console.error(result)
    status = 500
    body = result.toString()
  }
  return {status, body}
}

async function list(name:string):EndpointOutput {
  const result = await loadFileList(name)
  let status = 200
  let body = {shows:result}
  if (result instanceof Error) {
    console.error(result)
    status = 500
    body = result.toString()
  }
  return {status, body}
}

async function remove(name:string) {
  const date = (new Date()).getTime()
  const pth = resolve(dirname(name))
  const bsnm = basename(name)
  const newname = join(pth,'__'+bsnm)
  const result = await renameFile(name,newname)
  let status = 200
  let body = {name,newname}
  if (result instanceof Error) {
    console.error(result)
    status = 500
    body = result.toString()
  }
  return {status, body}

}

const endpointify = (result) => {
  let status = 200
  let body = result
  if (result instanceof Error) {
    console.error(result)
    status = 500
    body = result.toString()
  }
  return {status, body}
}

const DATADIR = './data/shows'

export async function get({params}):EndpointOutput {
  let {name} = params
  let result
  if (name === '__list__') {
      result = await list(DATADIR)
  } else {
    if (name.endsWith('.json')) result = await load(join(DATADIR,name))
    else if (name.endsWith('.md')) {

      name = name.replace('.md','.json')
      result = await load(join(DATADIR,name))
      let mrkdwn = ''
      for (const h of JSON.parse(result.body).slides) {
        for (const {text, notes, background} of h) {
           mrkdwn += '\n\n' + text
           if (background) {
             if (background.includes('/'))
             mrkdwn += '\n\n' + `![](${background})`
           }
           if (notes) mrkdwn += '\n\n----\n\n' + notes
        }
      }
      result.body = mrkdwn

    }
  }
  return result
}

export async function post({ params, body }):EndpointOutput {
  const {name} = params
  if (!name) return {status:415}
  const result = await save(join(DATADIR,name),JSON.parse(body))
	return result
}

export async function del({ params, body }):EndpointOutput {
  const {name} = params
  if (!name) return {status:415}
  const result = await remove(join(DATADIR,name))
	return result
}
