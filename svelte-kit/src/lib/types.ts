/*

data-background-video		A single video source, or a comma separated list of video sources.
data-background-video-loop	false	Flags if the video should play repeatedly.
data-background-video-muted	false	Flags if the audio should be muted.
data-background-size	cover	Use cover for full screen and some cropping or contain for letterboxing.
data-background-opacity	1	Opacity of the background video on a 0-1 scale. 0 is transparent and 1 is fully opaque.


data-background-image		URL of the image to show. GIFs restart when the slide opens.
data-background-size	cover	See background-size on MDN.
data-background-position	center	See background-position on MDN.
data-background-repeat	no-repeat	See background-repeat on MDN.
data-background-opacity	1	Opacity of the background image on a 0-1 scale. 0 is transparent and 1 is fully opaque.

data-background-color "aquamarine" or "rgb(70, 70, 255)"

*/

export type Background = { 
  url?: string
  color?: string
  type?: 'video' | 'image' | 'color'
}

export type Slide = {
  text: string
  transition: string
  background: Background
}

export interface Show {
  filename: string
  title?: string
  theme: string
  slides : array[Slide[]]

}

export type EndpointOutput = {
	status?: number;
	headers?: Headers;
	body?: string | Uint8Array | JSONValue;
};
