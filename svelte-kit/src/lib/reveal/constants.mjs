export const themes = [
'black', //	Black background, white text, blue links (default)
'white', //	White background, black text, blue links
'league', //	Gray background, white text, blue links
'beige', //	Beige background, dark text, brown links
'sky', //	Blue background, thin dark text, blue links
'night', //	Black background, thick white text, orange links
'serif', //	Cappuccino background, gray text, brown links
'simple', //	White background, black text, blue links
'solarized', //	Cream-colored background, dark green text, blue links
'blood', //	Dark background, thick white text, red links
'moon' //	Dark blue background, thick grey text, blue links
]

export const transitions = [
'none', //	Switch backgrounds instantly
'fade', //	Cross fade — default for background transitions
'slide', //	Slide between backgrounds — default for slide transitions
'convex', //	Slide at a convex angle
'concave', //	Slide at a concave angle
'zoom', //	Scale the incoming slide up so it grows in from the center of the screen
]
