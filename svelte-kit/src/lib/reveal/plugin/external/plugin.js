export const Externalcode = {
    id: "externalcode",
    init: (reveal) => {
      function fetchDataCodeSection(section) {
        const url = section.getAttribute("data-code");
        return fetch(url)
          .then((response) => response.text())
          .then((data) => {
            section.textContent = data;
          });
      }

      function fetchHtmlSection(section) {
        const url = section.getAttribute("html-insert");
        return fetch(url)
          .then((response) => response.text())
          .then((data) => {
            section.innerHTML = data;
          });
      }

      const { source } = reveal.getConfig()
      if (source) {
        const slides = document.querySelectorAll(".slides")[0]
        slides.innerHTML = source
      }

      const sections = document.querySelectorAll("[data-code], [html-insert]");
      const promiseArray = new Array(sections.length);
      /*
      for (let i = 0, len = sections.length; i < len; i++) {
        const section = sections[i];
        if (section.getAttribute("data-code") != null) {
          promiseArray[i] = fetchDataCodeSection(section);
        }

        if (section.getAttribute("html-insert") != null) {
          promiseArray[i] = fetchHtmlSection(section);
        }
      }
      */
      return Promise.all(promiseArray);
    },
  };
