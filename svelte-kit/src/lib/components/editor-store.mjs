import { writable, derived } from 'svelte/store';
//import { fetchShow, saveShow } from "./api.js";
const error = writable({})
const indexh = writable(0)
const indexv = writable(0)
const text = writable('### siin ei ole kala\n## see on\n# jäähall')
const transition = writable('fade-in slide-out')
const background = writable(undefined)

let timeoutId
const slide = derived([indexh,indexv,text, transition, background], ([h,v,t,r, b], set) => {
	clearTimeout(timeoutId);
	timeoutId = setTimeout(() => {
		const realslide = {indexh:h, indexv:v,text:t,transition:r,background:b}
		return set(realslide);
	}, 1000);
},0);



export { indexh, indexv, text, transition, background, slide, error }

/*
const storedShow = fetchShow()
export const show = writable(storedShow);
show.subscribe(value => { saveShow(value) });
*/
