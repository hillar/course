import marked from "$lib/tools/marked-2.0.5/marked.esm.js"

export const markdown2html = (s = '') => marked(s)

export function makeShowPreview(slideShow) {
  if (!slideShow) return ''
  let html = ""
  for (const horizontal of slideShow.slides) {
    const isVertical = horizontal.length > 1
    if (isVertical) html +=  `<section>\n`
    for (const slide of horizontal) {
      const {transition,background,text} = slide
      let bckgrnd = ''
      if (background) {
        const ends = background.split('.').pop()
        const images = ['png','jpg','gif','svg']
        const videos = ['mp4','mov','ogv','webm']
        if (images.includes(ends)) {
          bckgrnd = `data-background-image=${background}`
        } else if (videos.includes(ends)){
          bckgrnd = `data-background-video=${background} data-background-video-loop="true" data-background-video-muted="true"`
        } else {
          if (background.startsWith('http')) {
            bckgrnd = `data-background-iframe="${background}"`
          /*  
          } else if () {
            /*
            <iframe width="560" height="315" src="https://www.youtube.com/embed/3L5gIMHZ7_8?controls=0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            */

          } else bckgrnd = `data-background-color=${background}`
        }
        bckgrnd += ' data-background-opacity="0.5"'
      }
      html += `<section data-transition="${transition}" ${bckgrnd}>\n`
      html += markdown2html(text)
      html += "\n</section>\n"
    }
    if (isVertical) html += "</section>\n"
  }
  return html
}
